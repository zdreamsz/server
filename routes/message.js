var express = require('express');
var router = express.Router();
var async = require("async");

module.exports = function(app) {

    router.post('/', function (req, res) {
        var mysql = require('mysql');
        var pool = mysql.createPool({
            host: 'localhost',
            user: 'root',
            port: '8889',
            password: 'root',
            database: 'TeroTelegram',
        });

        function getLastMessages(messages) {
            for (var i = 0; i < messages.length - 1; i++)
                for (var j = i + 1; j < messages.length; j++) {
                    if (messages[i].ToUser == messages[j].ToUser) {
                        messages.splice(i, 1);
                        --i;
                    }
                    else if (messages[i].FromUser == messages[j].ToUser && messages[i].ToUser == messages[j].FromUser) {
                        messages.splice(i, 1);
                        --i;
                    }
                }

            /*while (i < 2000000000)
                i++;*/
            return messages;
        }

        pool.getConnection(function (error, connection) {
            async.waterfall([
                function (callback) {
                    connection.query("SELECT * FROM `users` WHERE `access_token` = '" + req.body.access_token + "'", function (error, rows) {
                        if (error || rows.length == 0) {
                            res.send();
                            return;
                        }

                        fromUser = rows[0].login;
                        callback(null, fromUser);
                    });
                },
                function (fromUser) {
                    connection.query("SELECT * FROM `message` WHERE `FromUser` = '" + fromUser + "' OR " + "`ToUser` = '" + fromUser + "'", function (error, rows) {
                        if (error) {
                            res.send();
                            return;
                        }

                        res.send({messages: getLastMessages(rows)});
                    });
                }
            ]);
        });
    });

    return router;
}

