var express = require('express');
var router = express.Router();


module.exports = function() {
    function random_access_token() {
        var str = "";
        for (var i = 0; i < 32; i++) {
            str += (Math.floor(Math.random(0, 1) * 17)).toString(32);
        }
        return str;
    }

    router.post('/', function (req, res) {
        var mysql = require('mysql');
        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            port: '8889',
            password: 'root',
            database: 'TeroTelegram',
        });

        var userData = {login: req.body.login, password: req.body.password, access_token: random_access_token()};
        connection.query("INSERT INTO users SET ?", userData, function (error, rows) {
            if (error) {
                if(error.code == "ER_DUP_ENTRY")
                    res.send({error: "A user with this number already exists!"});
                else
                    res.send({error: "Connect error!"});
                return;
            }
            res.send({access_token: userData.access_token});
        });

        connection.end();
    });

    return router;
};
